tool
extends Spatial

export(bool) var lobby:bool = false
export(bool) var respawn:bool = true
export(Color) var sky_color = Color() setget set_sky_color
export(String) var title = ""
export(String) var desc = ""
export(int) var music = -1

func new_car(c)->Node:
	match c:
		0:
			return CarLoader.small_car.instance()
		1:
			return CarLoader.police_car.instance()
		2:
			return CarLoader.truck.instance()
		3:
			return CarLoader.racecar.instance()
		4:
			return CarLoader.van.instance()
		_:
			return CarLoader.small_car.instance()

func rand_spawn_pos()->Vector3:
	return Vector3(rand_range($SpawnPositionLow.translation.x, $SpawnPositionHigh.translation.x), rand_range($SpawnPositionLow.translation.y, $SpawnPositionHigh.translation.y), rand_range($SpawnPositionLow.translation.z, $SpawnPositionHigh.translation.z))

func set_sky_color(c):
	EnvironmentManager.set_sky_color(c)
	sky_color = c

var players = {}
var joined = []
var custom_safe_function = false
func _ready():
	MusicManager.music(music)
	Overlay.fade_in()
	GameManager.room = self
	EnvironmentManager.set_sky_color(sky_color)
	Overlay.waiting_room(lobby)
	Overlay.update_players(-1)
	Overlay.timer(not lobby)
	Overlay.start_timer(not lobby)
	Overlay.set_start_timer("3")
	Overlay.game_name(title)
	Overlay.game_desc(desc)
	Overlay.safe(respawn and not lobby)
	Overlay.safe_count(0)
	Overlay.close_scoreboard()
	Networking.allow_join(lobby)
	if(lobby):
		Networking.connect("new_player", self, "new_player")
	Networking.connect("remove_player", self, "remove_player")
	GameManager.connect("new_spawn_pos", self, "new_spawn_pos")
	for id in Networking.player_info.keys():
		new_player(id)
	if(not Networking.my_info["dead"]):
		var car = new_car(Networking.my_info["car"])
		var my_id = get_tree().network_peer.get_unique_id()
		car.set_name("car"+str(my_id))
		car.id = my_id
		car.set_current(true)
		add_child(car)
		car.set_nick(Networking.my_info["name"])
		car.set_color(Networking.my_info["color"])
		car.set_highlight_color(Networking.my_info["highlight color"])
		car.set_tint_color(Networking.my_info["tint color"])
		car.global_transform.origin = rand_spawn_pos()
		GameManager.send_spawn_pos(car.global_transform.origin)
		players[my_id] = car
	else:
		spectate_next() # if im dead, spectate
	pause_mode = Node.PAUSE_MODE_PROCESS
	if(not lobby):
		get_tree().paused = true
		if(GameManager.is_server()):
			rpc("request_joined")
			recv_join(1)
		else:
			rpc_id(1, "recv_join")
	if(lobby and GameManager.is_server()):
		Matchmaking.connect("timeout", self, "matchmaking_timeout")
		Matchmaking.connect("matchmaking_unavailable", self, "matchmaking_unavailable")
		Matchmaking.server()
	_setup()

func _setup():
	pass

func matchmaking_timeout():
	Networking.error = "LOBBY TIMED OUT"
	Loader.goto_scene("res://menus/MainMenu.tscn")

func matchmaking_unavailable():
	Networking.error = "MATCHMAKING UNAVAILABLE"
	Loader.goto_scene("res://menus/MainMenu.tscn")

remote func request_joined():
	rpc_id(1, "recv_join")

remote func recv_join(id:int=-1):
	if(id==-1):
		id = get_tree().get_rpc_sender_id()
	else:
		id = get_tree().get_network_unique_id()
	if(joined.find(id) == -1):
		joined.append(id)
	if(len(joined) >= len(players)):
		rpc("start_game")

var start_timer:Timer
sync func start_game():
	start_timer = Timer.new()
	start_timer.one_shot = true
	start_timer.autostart = false
	start_timer.wait_time = 3
	add_child(start_timer)
	start_timer.connect("timeout", self, "start_timer_timeout")
	start_timer.start()
	Overlay.start_timer(true)

func update_start_timer(): # gets called every frame
	if(start_timer != null):
		Overlay.set_start_timer(str(int(start_timer.time_left)+1))

func start_timer_timeout():
	if(GameManager.is_server()):
		rpc("sync_game_start_timeout")

sync func sync_game_start_timeout():
	Overlay.start_timer(false)
	get_tree().paused = false

func _input(_event:InputEvent):
	if(cur_spectating != -1):
		if(Input.is_action_just_pressed("spectate_next")):
			spectate_next()
		if(Input.is_action_just_pressed("spectate_prev")):
			spectate_prev()

var cur_spectating:int = -1
func spectate_next():
	update_players()
	cur_spectating += 1
	cur_spectating = wrapi(cur_spectating, 0, len(players))
	update_spectating()

func spectate_prev():
	update_players()
	cur_spectating -= 1
	cur_spectating = wrapi(cur_spectating, 0, len(players))
	update_spectating()

func update_spectating():
	if(len(players) == 0):
		return
	var id = players.keys()[cur_spectating]
	for p in players.keys():
		players[p].set_current(p==id)

func new_player(id):
	if(lobby and GameManager.is_server() and len(Networking.player_info)+1 >= Networking.MAX_PLAYERS):
		Matchmaking.unregister_server()
	if(Networking.player_info[id]["dead"]):
		return # dont spawn the car if the player is already dead
	Overlay.update_players(id)
	var car = new_car(Networking.player_info[id]["car"])
	players[id] = car
	car.id = id
	car.set_current(false)
	car.set_name("car"+str(id))
	add_child(car)
	car.set_dummy(true)
	if(GameManager.spawn_positions.has(id)):
		car.global_transform.origin = GameManager.spawn_positions[id]
	car.set_nick(Networking.player_info[id]["name"])
	car.set_color(Networking.player_info[id]["color"])
	car.set_highlight_color(Networking.player_info[id]["highlight color"])
	car.set_tint_color(Networking.player_info[id]["tint color"])
	_car_setup(car)

func new_spawn_pos(id):
	players[id].global_transform.origin = GameManager.spawn_positions[id]

func _car_setup(_c:Node):
	pass

func remove_player(id):
	if(lobby and GameManager.is_server() and len(Networking.player_info)+1 == Networking.MAX_PLAYERS-1):
		Matchmaking.server()
	if(players.has(id)):
		players[id].queue_free()
		players.erase(id)

func update_players():
	for k in players.keys():
		if(not is_instance_valid(players[k])):
			players.erase(k)

func _on_TeleportTimer_timeout():
	update_players()
	for p in players.keys():
		if(not players[p].dummy and players[p].global_transform.origin.y < $VoidPosition.translation.y):
			if(respawn):
				players[p].linear_velocity = Vector3()
				players[p].angular_velocity = Vector3()
				players[p].global_transform.origin = rand_spawn_pos()
				players[p].global_transform.basis = Basis()
			else:
				Networking.kill(p)
				GameManager.kill(p)
				die(players[p])
				remove_player(p)

func die(car):
	update_players()
	if(car.is_current()):
		spectate_next()

export(float) var time_limit = 60*0.5
var time = 0
var done = false
func _process(delta):
	if(Engine.editor_hint):
		return
	update_start_timer()
	if(lobby or done or get_tree().paused):
		return
	time = min(time+delta, time_limit)
	Overlay.set_time(time_limit-time)
	if(not GameManager.is_server()):
		return
	if(time >= time_limit):
		done = true
		GameManager.send_timeout()

func get_safe():
	pass

func get_unsafe():
	pass
