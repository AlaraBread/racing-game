tool
extends HSlider

export(String) var text = "" setget set_text

func set_text(n:String):
	text = n
	$Label.text = n

func _ready():
	$Label.text = text

func _on_Slider_focus_entered():
	$ColorRect.visible = true

func _on_Slider_focus_exited():
	$ColorRect.visible = false
