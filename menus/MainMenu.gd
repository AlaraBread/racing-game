tool
extends Control

onready var cars = [
		$ViewportContainer/Viewport/sedan,
		$ViewportContainer/Viewport/police,
		$ViewportContainer/Viewport/truck,
		$ViewportContainer/Viewport/race,
		$ViewportContainer/Viewport/van
	]

func _ready():
	if(Engine.editor_hint):
		return
	MusicManager.music(1)
	Overlay.fade_in()
	GameManager.reset()
	Networking.connect("connected_result", self, "_connected_result")
	Networking.connect("server_result", self, "server_result")
	Matchmaking.connect("new_server", self, "matchmaking_result")
	Matchmaking.connect("matchmaking_unavailable", self, "matchmaking_unavailable")
	Matchmaking.connect("connected", self, "matchmaking_connected")
	Matchmaking.connect("cancelled", self, "cancelled")
	Overlay.connect("settings_closed", self, "settings_closed")
	Overlay.ingame(false)
	Overlay.waiting_room(false)
	$Main/Play.grab_focus()
	for c in range(len(cars)):
		var a = (len(cars)-c)*(2*PI/len(cars))
		cars[c].translation = cars[c].translation.rotated(Vector3.UP, a)
		cars[c].rotation.y += a
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	if(Networking.error != ""):
		info(Networking.error)
		Networking.error = ""
	Networking.update_my_info()

func _on_Play_pressed():
	$Main/PlayMenu/Host.grab_focus()

func _on_Customize_pressed():
	$AnimationPlayer.play("customize_open")
	$Customize/ExitCustomize.grab_focus()

func _on_Quit_pressed():
	get_tree().quit()

func _on_Host_focus_entered():
	$Main/PlayMenu.visible = true
	$Main/PlayMenu/PortMenu.visible = true

func _on_Client_focus_entered():
	$Main/PlayMenu.visible = true

func _on_JoinAddress_focus_entered():
	$Main/PlayMenu.visible = true

func _on_Host_focus_exited():
	$Main/PlayMenu.visible = false
	$Main/PlayMenu/PortMenu.visible = false

func _on_Client_focus_exited():
	$Main/PlayMenu.visible = false

func _on_ExitCustomize_pressed():
	$AnimationPlayer.play("customize_close")
	$Main/Customize.grab_focus()

func _on_Host_pressed():
	Networking.update_my_info()
	info("STARTING SERVER...", -1)
	grab_info()
	Networking.server()

func server_result(err):
	if(err == OK):
		GameManager.start_lobby()
	else:
		info("ERROR STARTING GAME")
		revert_focus()

func _on_Client_pressed():
	Matchmaking.client()
	info("CONNECTING TO MATCHMAKING SERVER...", -1)
	grab_info()

var connecting_ip:String = ""
func matchmaking_result(ip):
	connecting_ip = ip
	Networking.update_my_info()
	var err = Networking.client(ip)
	info("JOINING GAME...", -1)
	if(err != OK):
		_connected_result(false)

func matchmaking_unavailable():
	info("MATCHMAKING UNAVAILABLE")
	revert_focus()

func matchmaking_connected():
	info("SEARCHING FOR GAME...", -1, true)

func _connected_result(r):
	if(r):
		GameManager.start_lobby()
	else:
		Matchmaking.report_server(connecting_ip)
		connecting_ip = ""
		info("ERROR JOINING GAME")

func _on_Settings_pressed():
	Overlay.settings(true)

func settings_closed():
	$Main/Settings.grab_focus()

func info(t:String, timeout:float=3, cancel:bool=false):
	$Info/Label.text = t
	if(timeout > 0):
		$InfoTimer.wait_time = timeout
		$InfoTimer.start()
	$Info/InfoCancel.visible = cancel
	if(cancel):
		$Info/InfoCancel.grab_focus()
	$Info.visible = true

func _on_InfoTimer_timeout():
	$Info.visible = false

func _on_Info_gui_input(event):
	if(event is InputEventMouseButton):
		if($InfoTimer.time_left != 0):
			_on_InfoTimer_timeout()
			$InfoTimer.stop()

onready var prev_focus = $Main/Play
func grab_info():
	prev_focus = get_focus_owner()
	prev_focus.release_focus()

func revert_focus():
	if(prev_focus != null):
		prev_focus.grab_focus()

func _on_InfoCancel_pressed():
	Matchmaking.cancel()

func cancelled():
	_on_InfoTimer_timeout()
	$Main/Play.grab_focus()
