extends LineEdit

func _on_LineEdit_text_changed(new_text):
	var c:int = caret_position
	text = new_text.to_upper()
	caret_position = c
	GameSaver.set("name", text)
