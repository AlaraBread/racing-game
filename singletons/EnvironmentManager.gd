tool
extends Node

func _ready():
	set_sky_color(Color(0.546021, 0.943279, 0.984375))

var env = preload("res://default_env.tres")
func set_sky_color(c:Color):
	c.v = clamp(c.v, 0, 1)
	env.ambient_light_color = c
	env.background_sky.sky_top_color = c
	c.v *= 0.75
	env.background_sky.sky_horizon_color = c
	env.background_sky.ground_horizon_color = c
	env.background_sky.ground_bottom_color = c
