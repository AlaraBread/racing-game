extends Control

func new_car(c)->Node:
	match c:
		0:
			return preload("res://cars/Car.tscn").instance()
		1:
			return preload("res://cars/PoliceCar.tscn").instance()
		2:
			return preload("res://cars/Truck.tscn").instance()
		_:
			return preload("res://cars/Car.tscn").instance()

var new_game:bool = true
func show_scores(scores, cont:bool):
	new_game = cont
	if(GameManager.is_server()):
		cont = false
	$ScoreboardContinue.visible = not cont
	$WaitingLabel.visible = cont
	for c in $Players.get_children():
		c.queue_free()
	for i in range(len(scores)):
		var info:Dictionary
		if(Networking.player_info.has(scores[i][0])):
			info = Networking.player_info[scores[i][0]]
		else:
			info = Networking.my_info
		var display = preload("res://menus/ScoreboardPlayer.tscn").instance()
		display.set_nick(info["name"])
		display.set_score(scores[i][1])
		$Players.add_child(display)

func _on_ScoreboardContinue_pressed():
	visible = false
	if(new_game):
		GameManager.start_game()
