extends Node

const MINIMUM_PLAYERS:int = 2
var room:Node
func _ready():
	randomize()

func is_server()->bool:
	if(get_tree().network_peer != null):
		return get_tree().is_network_server()
	return false

func start_lobby():
	reset()
	Loader.goto_scene("res://maps/WaitingRoom.tscn")

func random_game()->String:
	match randi()%5:
		0:
			return "res://maps/Race1.tscn"
		1:
			return "res://maps/Race2.tscn"
		2:
			return "res://maps/Race3.tscn"
		3:
			return "res://maps/KOTH.tscn"
		4:
			return "res://maps/Tag.tscn"
		_:
			return "res://maps/Race1.tscn"

var game_running := false
var num_rounds := 0
var safe_count:int = 10
func start_game():
	if(not is_server()):
		return
	safe_count = int(ceil(count_alive()*0.5))
	rpc("load_round", random_game(), safe_count)

sync func load_round(path:String, sc:int):
	if(not game_running):
		game_running = true
		safe_count = sc
		Loader.goto_scene(path)

func finish(score:String):
	if(get_tree().get_network_unique_id() == 1):
		recv_finish(score, 1)
	else:
		rpc_id(1, "recv_finish", score)

func kill(id:int):
	if(get_tree().get_network_unique_id() == 1):
		recv_kill(1)
	else:
		rpc_id(1, "recv_kill", id)

func send_timeout():
	rpc_id(1, "game_timeout")

func kill_unsafe()->bool:
	var peers: = get_tree().get_network_connected_peers()
	peers.append(get_tree().get_network_unique_id())
	for id in peers:
		var safe := false
		for f in finish_order:
			if(f[0] == id):
				safe = true
				break
		if(not safe):
			Networking.kill(id)
	if(count_alive() == 0):
		var top_players := []
		for i in range(LEADERBOARD_SIZE):
			if(i < len(finish_order)):
				top_players.append([finish_order[i], ""])
		rpc("end_round", top_players, false)
		return true
	return false

func count_alive():
	var alive:int = 0
	if(not Networking.my_info["dead"]):
		alive += 1
	for id in Networking.player_info.keys():
		if(not Networking.player_info[id]["dead"]):
			alive += 1
	return alive

sync func game_timeout():
	if(not game_running):
		return
	game_running = false
	var top_players := []
	if(room.custom_safe_function):
		var safe_order = room.get_safe()
		for i in range(LEADERBOARD_SIZE):
			if(i < len(safe_order)):
				top_players.append([safe_order[i], ""])
		for id in room.get_unsafe():
			Networking.kill(id)
		rpc("end_round", top_players, count_alive() > 1)
		return
	elif(room.respawn):
		for i in range(LEADERBOARD_SIZE):
			if(i < len(finish_order)):
				top_players.append(finish_order[i])
		#kill unsafe players
		var all_killed: = kill_unsafe()
		if(not all_killed):
			rpc("end_round", top_players, count_alive() > 1)
		num_rounds += 1
		return
	else:
		#get leaderboard from players not in finish_order
		var other_peers: = get_tree().get_network_connected_peers()
		for i in other_peers:
			top_players.append([i, ""])
		top_players.append([get_tree().get_network_unique_id(), ""])
		for i in top_players:
			if(Networking.is_dead(i[0])):
				top_players.erase(i)
		num_rounds += 1
		rpc("end_round", top_players, count_alive() > 1)
		return

remote func recv_kill(id:int=-1):
	if(not is_server()):
		return
	if(not game_running):
		return
	if(id==-1):
		id = get_tree().get_rpc_sender_id()
	Networking.kill(id)
	finish_order.insert(0, [id, ""])
	if(count_alive() <= safe_count):
		game_running = false
		#get leaderboard from players not in finish_order
		var top_players: = []
		var other_peers: = get_tree().get_network_connected_peers()
		for i in other_peers:
			top_players.append([i, ""])
		top_players.append([get_tree().get_network_unique_id(), ""])
		for i in top_players:
			if(Networking.is_dead(i[0])):
				top_players.erase(i)
		num_rounds += 1
		rpc("end_round", top_players, count_alive() > 1)

const LEADERBOARD_SIZE:int = 5
var finish_order := []
remote func recv_finish(score:String, id:int=-1):
	if(not is_server()):
		return
	if(not game_running):
		return
	if(id==-1):
		id = get_tree().get_rpc_sender_id()
	finish_order.append([id, score])
	rpc("recv_safe_count", len(finish_order))
	if(len(finish_order) >= safe_count):
		game_running = false
		#get leaderboard
		var top_players := []
		for i in range(LEADERBOARD_SIZE):
			if(i < len(finish_order)):
				top_players.append(finish_order[i])
		num_rounds += 1
		#kill unsafe players
		var all_killed: = kill_unsafe()
		if(not all_killed):
			rpc("end_round", top_players, count_alive() > 1)

sync func recv_safe_count(c:int):
	Overlay.safe_count(c)

sync func end_round(top, cont:bool):
	game_reset()
	Overlay.scoreboard(top, cont)
	if(not cont):
		Overlay.ingame(false)
		Networking.quit()
		Loader.goto_scene("res://menus/MainMenu.tscn")

func game_reset():
	game_running = false
	finish_order = []
	spawn_positions = {}

func reset():
	game_reset()
	num_rounds = 0

func send_spawn_pos(pos:Vector3):
	rpc("recv_spawn_pos", pos)

var spawn_positions = {}
signal new_spawn_pos(id)
remote func recv_spawn_pos(pos:Vector3):
	var id = get_tree().get_rpc_sender_id()
	spawn_positions[id] = pos
	emit_signal("new_spawn_pos", id)
