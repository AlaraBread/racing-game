extends Node

# The URL we will connect to
export var websocket_url = "ws://73.194.216.96:5874"

# Our WebSocketClient instance
var _client:WebSocketClient = WebSocketClient.new()

func _ready():
	# Connect base signals to get notified of connection open, close, and errors.
	_client.connect("connection_closed", self, "_closed")
	_client.connect("connection_error", self, "_closed")
	_client.connect("connection_established", self, "_connected")
	# This signal is emitted when not using the Multiplayer API every time
	# a full packet is received.
	# Alternatively, you could check get_peer(1).get_available_packets() in a loop.
	_client.connect("data_received", self, "_on_data")
	set_process(false)

var connected:bool = false
signal matchmaking_unavailable
func connect_to_server():
	# Initiate connection to the given URL.
	var err = _client.connect_to_url(websocket_url)
	if err != OK:
		emit_signal("matchmaking_unavailable")
		set_process(false)
	else:
		set_process(true)

func report_server(ip:String):
	if(connected):
		_client.disconnect_from_host()
	send = "REPORT " + ip
	connect_to_server()

var send:String = ""
func server():
	if(connected):
		_client.disconnect_from_host()
	send = "SERVER"
	connect_to_server()

var unregister = false
func unregister_server():
	unregister = true
	if(connected):
		_client.get_peer(1).put_packet("UNREGISTER".to_utf8())

func client():
	if(connected):
		_client.disconnect_from_host()
	send = "CLIENT"
	connect_to_server()

signal timeout
signal cancelled
func _closed(_was_clean = false):
	# was_clean will tell you if the disconnection was correctly notified
	# by the remote peer before closing the socket.
	if(not cancelling):
		if(not unregister):
			emit_signal("timeout")
		if(not connected):
			emit_signal("matchmaking_unavailable")
	else:
		emit_signal("cancelled")
	unregister = false
	connected = false
	cancelling = false
	set_process(false)

var cancelling = false
func cancel():
	cancelling = true
	_client.disconnect_from_host()

signal connected
func _connected(_proto = ""):
	# This is called on connection, "proto" will be the selected WebSocket
	# sub-protocol (which is optional)
	# You MUST always use get_peer(1).put_packet to send data to server,
	# and not put_packet directly when not using the MultiplayerAPI.
	connected = true
	emit_signal("connected")
	_client.get_peer(1).put_packet(send.to_utf8())

signal new_server(ip)
func _on_data():
	# to receive data from server, and not get_packet directly when not
	# using the MultiplayerAPI.
	var data:String = _client.get_peer(1).get_packet().get_string_from_utf8()
	if(data == "DISCONNECT"):
		emit_signal("timeout")
		Networking.error = "NOT REACHABLE FROM THE INTERNET"
		_client.disconnect_from_host()
	else:
		emit_signal("new_server", data)

func _process(_delta):
	# Call this in _process or _physics_process. Data transfer, and signals
	# emission will only happen when calling this function.
	_client.poll()
