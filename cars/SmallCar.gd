extends "res://cars/Car.gd"

export(float) var jump_force = 1000
func _special_just_pressed():
	if(is_on_floor):
		rpc("sync_jump")
		use_special()

sync func sync_jump():
	linear_velocity += global_transform.basis.y*jump_force
	$JumpPlayer.play()

func _special_just_released():
	pass

func _special_pressed(_delta:float):
	pass

func _special_process(_delta:float):
	Overlay.highlight(can_special and is_on_floor)
