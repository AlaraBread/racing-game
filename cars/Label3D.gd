extends Spatial

export(NodePath) var follow_path:NodePath
var follow:Node
var offset:Vector3

func _ready():
	follow = get_node(follow_path)
	offset = global_transform.origin-follow.global_transform.origin

func _process(_delta):
	global_transform.origin = follow.global_transform.origin+offset

func set_text(t:String):
	$Viewport/Label.text = t
