extends "res://cars/Car.gd"

export(float) var repel_force:float = 100
export(float) var bubble_alpha:float = 0.0 setget set_bubble_alpha

func set_bubble_alpha(a:float):
	if(is_inside_tree()):
		$RepelBubble.get_active_material(0).albedo_color.a = a

func _set_color(c:Color):
	c.a = $RepelBubble.get_active_material(0).albedo_color.a
	$RepelBubble.get_active_material(0).albedo_color = c

func _special_just_pressed():
	rpc("sync_repel")
	use_special()

sync func sync_repel():
	$RepelBubble/AnimationPlayer.play("repel")
	$RepelPlayer.play()
	for b in $RepelArea.get_overlapping_bodies():
		if(b.is_in_group("car") and b != self):
			b.apply_impulse(Vector3(), repel_force*global_transform.origin.direction_to(b.global_transform.origin))

func _special_just_released():
	pass

func _special_pressed(_delta:float):
	pass

func _special_process(_delta:float):
	Overlay.highlight(can_special)
