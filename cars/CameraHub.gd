extends Spatial

const SCROLL_DIST := 0.25
const OFFSET := Vector3(0, 2, 0)

var mouse := Vector2()
var active := false
func set_active(a:bool)->void:
	active = a

var invert_y := false
func set_invert_y(y:bool)->void:
	invert_y = y

var sensitivity := 0.01
var initial_dist:float

onready var parent := get_node("../../../..")
onready var camera := $Camera
onready var camera_ray := $CameraRay
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	Overlay.connect("sensitivity_changed", self, "sensitivity_changed")
	sensitivity_changed(GameSaver.get("sensitivity"))
	initial_dist = camera.translation.z
	set_dist(initial_dist)
	camera_ray.add_exception(parent)
	yield(get_tree(), "idle_frame")
	set_active(true)
	Overlay.connect("pause", self, "pause_toggled")

func sensitivity_changed(s:float):
	sensitivity = s

func pause_toggled(p):
	set_active(not p)
	if(p):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _input(event:InputEvent):
	if(not camera.current):
		return
	if(not active):
		return
	if(event is InputEventMouseMotion):
		if(invert_y):
			event.relative.y *= -1
		mouse += event.relative*sensitivity
	if(active and Input.is_action_just_pressed("zoom_in")):
		set_dist(dist-SCROLL_DIST)
	if(active and Input.is_action_just_pressed("zoom_out")):
		set_dist(dist+SCROLL_DIST)

func _process(_delta:float):
	global_transform.origin = parent.global_transform.origin+OFFSET
	if(not camera.current):
		return
	if(Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED):
		return
	rotation.x = clamp(rotation.x, (-PI/2.25)+abs(mouse.y), (PI/2)-abs(mouse.y))
	rotate_object_local(Vector3.RIGHT, -mouse.y)
	rotate(Vector3.UP, -mouse.x)
	mouse = Vector2()
	rotation.z = 0
	if(camera_ray.is_colliding()):
		camera.translation.z = camera_ray.get_collision_point().distance_to(global_transform.origin)-camera_buffer
	else:
		camera.translation.z = initial_dist-camera_buffer

var camera_buffer := 1.0
var dist := 0.0
const MIN_DIST := 2.0
const MAX_DIST := 10.0
func set_dist(d:float)->void:
	d = clamp(d, MIN_DIST, MAX_DIST)
	dist = d
	camera.translation.z = d-camera_buffer
	camera_ray.cast_to.z = d
	initial_dist = d

var prev_rotation
func car_rotation(r):
	if(prev_rotation != null):
		rotation.y += r-prev_rotation
	prev_rotation = r

func stop_rotation():
	prev_rotation = null
